
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrId = "";

var whoData = 'http://apps.who.int/gho/athena/data/GHO/BP_06.json?profile=simple&filter=SEX:*;COUNTRY:-;REGION:GLOBAL';


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  sessionId = getSessionId();
    if(stPacienta==1) {
	    var ime = "Miha";
	    var priimek = "Mojster";
        var datumRojstva = "1964-6-12" + "T00:00:00.000Z";
    } else if (stPacienta==2) {
        var ime = "Hitri";
	    var priimek = "Dirkač";
        var datumRojstva = "1985-3-6" + "T00:00:00.000Z";
    } else {
        var ime = "Krojaček";
	    var priimek = "Hlaček";
        var datumRojstva = "1986-12-1" + "T00:00:00.000Z";
    }

	
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    async: true,
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            async: true,
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    var option = "<option value="+ehrId+">"+ime+" "+priimek+"</option>";
		                    $("#preberiObstojeciEHR").append(option);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
for(var x=0; x<10;x--) {	
	var datumInUra = "2018-05-"+(10+x)+"T00:00:00.000Z";
	var diastolicniKrvniTlak = Math.floor(Math.random() * 120) + 50;
	var sistolicniKrvniTlak = Math.floor(Math.random() * 130) + 50 + diastolicniKrvniTlak;

	
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    //"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    async: true,
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		       
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	
}
}

function generiraj() {
    for(var i=0; i<3;i++) {
        generirajPodatke(i);
    }    
}

function preberiEHRodBolnika() {//prijavaUporabnika
    sessionId = getSessionId();

	ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Prijavljen je uporabnik '" + party.firstNames + " " +
          party.lastNames + "'.</span>");
                var x = document.getElementById("poLogin");
                x.style.display = "block";
                 var x = document.getElementById("poLoginBt");
                x.style.display = "block";
                x = document.getElementById("predLogin");
                x.style.display = "none";
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}

  return ehrId;
}

function kreirajEHRzaBolnika() {//dodajUporabnika
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                     var option = "<option value="+ehrId+">"+ime+" "+priimek+"</option>";
		                    $("#preberiObstojeciEHR").append(option);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function preberiEHRodBolnika() {//prijavaUporabnika
    sessionId = getSessionId();

	ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Prijavljen je uporabnik '" + party.firstNames + " " +
          party.lastNames + "'.</span>");
                var x = document.getElementById("poLogin");
                x.style.display = "block";
                 var x = document.getElementById("poLoginBt");
                x.style.display = "block";
                x = document.getElementById("predLogin");
                x.style.display = "none";
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function odjava() {//odjavaUporabnika
    sessionId = getSessionId();
    
    ehrId = "";
    
    window.location.reload(true);
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {//dodajMeritveKrvnegaTlaka
	sessionId = getSessionId();

	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if ((datumInUra.length==0) || (sistolicniKrvniTlak.length==0) || (diastolicniKrvniTlak.length==0)) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/**
 * Prebere zapise za krvni pritisk
 */
function preberiMeritveVitalnihZnakov() {//preberiMeritveKrvnegaTlaka
	sessionId = getSessionId();

	var tip = "blood_pressure";

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>Krvni tlak</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    	    var data = [0,0,0,0,0,0,0,0,0,0];
					    	var results = "<div>";
					    	var parseTime = d3.timeParse("%Y-%B-%d");
					        for (var i in res) {
					            var date = res[i].time.substring(0,10);
					            var mesec;
					            
					            if(date.substring(5,7) == 01) {
					                mesec = "Jan";
					            } else if(date.substring(5,7) == 02) {
					                mesec = "Feb";
					            } else if(date.substring(5,7) == 03) {
					                mesec = "Mar";
					            } else if(date.substring(5,7) == 04) {
					                mesec = "Apr";
					            } else if(date.substring(5,7) == 05) {
					                mesec = "May";
					            } else if(date.substring(5,7) == 06) {
					                mesec = "June";
					            } else if(date.substring(5,7) == 07) {
					                mesec = "July";
					            } else if(date.substring(5,7) == 08) {
					                mesec = "Aug";
					            } else if(date.substring(5,7) == 09) {
					                mesec = "Sept";
					            } else if(date.substring(5,7) == 10) {
					                mesec = "Oct";
					            } else if(date.substring(5,7) == 11) {
					                mesec = "Nov";
					            } else if(date.substring(5,7) == 12) {
					                mesec = "Dec";
					            } 
					            console.log(date.substring(0,4)+"-"+mesec+"-"+date.substring(8,10));
					            var xclose = +res[i].systolic;
					            data[i] = {date:parseTime(date.substring(0,4)+"-"+mesec+"-"+date.substring(8,10)),close:xclose};
					            var meja = "";
					            if((res[i].diastolic>90)||(res[i].systolic>140)) {
					                meja = "PREVISOK";
					            } else if((res[i].diastolic<60)||(res[i].systolic<110)) {
					                meja = "PRENIZEK";
					            } else {
					                meja = "DOBER";
					            }
					            results += "<div><button type='button'class='btn btn-default selected' onclick='dodajPodrobnosti("+i+")' style='width:100%'>" +date +
                      " : " + meja + "</button><span id="+i+"></span></div>";
					        }
					        results += "</div>";
					        $("#rezultatMeritveVitalnihZnakov").append(results);

                            var svg = d3.select("svg"),
                                margin = {top: 20, right: 20, bottom: 30, left: 50},
                                width = +svg.attr("width") - margin.left - margin.right,
                                height = +svg.attr("height") - margin.top - margin.bottom,
                                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                            
                            
                            
                            var x = d3.scaleTime()
                                .rangeRound([0, width]);
                            
                            var y = d3.scaleLinear()
                                .rangeRound([height, 0]);
                            
                            var area = d3.area()
                                .x(function(d) { return x(d.date); })
                                .y1(function(d) { return y(d.close); });
                            
                            
                              
                             
                            
                              x.domain(d3.extent(data, function(d) { return d.date; }));
                              y.domain([0, d3.max(data, function(d) { return d.close; })]);
                              area.y0(y(0));
                            
                              g.append("path")
                                  .datum(data)
                                  .attr("fill", "steelblue")
                                  .attr("d", area);
                            
                              g.append("g")
                                  .attr("transform", "translate(0," + height + ")")
                                  .call(d3.axisBottom(x));
                            
                              g.append("g")
                                  .call(d3.axisLeft(y))
                                .append("text")
                                  .attr("fill", "#000")
                                  .attr("transform", "rotate(-90)")
                                  .attr("y", 6)
                                  .attr("dy", "0.71em")
                                  .attr("text-anchor", "end")
                                  .text("Krvni tlak (mm Hg)");
                           
                                
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
				    	}
					   },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function dodajPodrobnosti(id) {
    	sessionId = getSessionId();

	var tip = "blood_pressure";

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				        if (res.length > 0) {
				        /*    $.ajax({
				          *      url: whoData,
				          *      type: 'GET',
				          *      success: function (who) {
				          *          console.log(who);
					       */         var results = "<table class='table table-striped table-hover'>"+
					                "<tr><td>Datum in ura</td><td class='text-right'>"+res[id].time+"</td></tr>"+
					                "<tr><td>Sistolični krvni tlak</td><td class='text-right'>"+res[id].systolic+"</td></tr>"+
					                "<tr><td>Diastolični Krvni Tlak</td><td class='text-right'>"+res[id].diastolic+"</td></tr>"+
					                "<tr><td>Povprečni Sistolični po svetu</td><td class='text-right'>Telesna temperatura</td></tr></table>";
					                document.getElementById(id).innerHTML += results;
				        /*        }
				         *   });
				    */	} else {
				    		$("#"+id).html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
				    	}
					   },
				    error: function() {
				    	$("#"+id).html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
	    	},
	    	error: function(err) {
	    		$("#"+id).html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});